# markup-creator [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Get JSON Spew markup

## Installation

```sh
$ npm install --save markup-creator
```

## Usage

```js
import markupCreator from 'markup-creator';

markupCreator.rawContentStateToHtml(rawContentState, config);
```

Use [this](https://github.com/sstur/draft-js-export-html#options) format of configuration.

## License

[npm-image]: https://badge.fury.io/js/markup-creator.svg
[npm-url]: https://npmjs.org/package/markup-creator
[travis-image]: https://travis-ci.org/FTBpro/markup-creator.svg?branch=master
[travis-url]: https://travis-ci.org/FTBpro/markup-creator
[daviddm-image]: https://david-dm.org/FTBpro/markup-creator.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/FTBpro/markup-creator
