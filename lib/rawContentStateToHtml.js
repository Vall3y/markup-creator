import { convertFromRaw } from 'draft-js';
import { stateToHTML } from 'draft-js-export-html';

export default function rawContentStateToHtml(contentStateJson, config) {
  const contentState = convertFromRaw(contentStateJson);
  return stateToHTML(contentState, config);
}
