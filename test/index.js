import assert from 'assert';
import markupCreator from '../lib';

describe('markup-creator', () => {
  it('has rawContentStateToHtml function', () => {
    assert({}.hasOwnProperty.call(markupCreator, 'rawContentStateToHtml'),
      'Module has no rawContentStateToHtml function');
  });
});
