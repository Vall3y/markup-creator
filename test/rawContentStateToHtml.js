import assert from 'assert';
import rawContentStateToHtml from '../lib/rawContentStateToHtml';

describe('rawContentStateToHtml', () => {
  it('returns expected html', () => {
    const json = {
      entityMap: {},
      blocks: [
        {
          key: '7uepr',
          text: 'text',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [],
        },
      ],
    };

    const expectedHtml = '<p>text</p>';
    const result = rawContentStateToHtml(json);
    assert(result === expectedHtml, 'Got unexpected html output');
  });
});
